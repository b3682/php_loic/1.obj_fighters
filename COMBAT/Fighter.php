<?php 

class Fighter {

    private $name; // string
    private  $hp;  // int
    private $attack; // int
    private $defense = 0; // int
    private $init  =  0; // int
    private $spell = "confusion"; //string


    //  construction de notre fighter
    public function __construct($name,$hp,$attack,$defense)
    {       
        $this->name = $name;
        $this->hp = $hp;
        $this->attack = $attack;
        $this->defense = $defense;

    }


    // on recup  les pv de base du combatant
    public function getHp()
    {
        $hp = $this->hp;
        return $hp;
    }

    // on recup  le nom de base du combatant
    public function get_name()
    {
        $name = $this->name;
        return $name;
    }

    // on recup ses  pv une fois une action effectuée dessus
    public function updateHp($hp)
    {
        $this->hp = $hp;
        return $hp;
        
    }


    // on recup l'attaque de notre fighter
    public function getAttack()
    {
        return $this->attack;
    }

    // on recup la def de notre fighter
    public function getDefense()
    {
        return $this->defense;
    }

    // on recup  le sort de base du combatant
    public function get_spell()
    {
        $spell = $this->spell;
        echo $spell;
    }


    // on lance les dés d'initiative
    public function getInit()
    {
        $this->init =  rand(0,10);
        return $this->init;
    }



    



    
}

?>